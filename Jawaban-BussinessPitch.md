# No.1 Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
A. Use Case(User)
[User Use Case](Use_Case_User_.pdf)

B. Use Case(Perusahaan)
[Company Use Case](Use_Case_Perusahaan_.pdf)

# No.2 Mampu Class Diagram dari keseluruhan Use Case produk digital
[COC_ClassDiagram](ClassDiagramCOC.png)

# No.3 Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Berikut adalah penjelasan penerapan setiap poin dari SOLID Design Principles:

**Single Responsibility Principle (SRP):**

Setiap interface dan kelas (WebService) bertanggung jawab terhadap satu spesifik tugas yang terkait dengan repositori data yang berbeda.
Setiap fungsi di dalam kelas WebService memiliki tanggung jawab yang terkait dengan operasi CRUD pada repositori yang sesuai.

**Open-Closed Principle (OCP):**

Program ini terbuka untuk perluasan melalui penggunaan interface. Interface-interface tersebut dapat diperluas dengan menambahkan metode baru sesuai kebutuhan tanpa mengubah kode yang sudah ada.
Kelas WebService mengimplementasikan semua interface terkait, sehingga dapat memperluas fungsi dengan menambahkan implementasi metode baru pada kelas tersebut.

**Interface Segregation Principle (ISP):**

Setiap interface hanya berisi metode-metode yang relevan untuk repositori data yang terkait. Dengan memisahkan metode-metode sesuai kebutuhan, klien yang mengimplementasikan interface tidak dipaksa untuk mengimplementasikan metode yang tidak diperlukan.

**Dependency Inversion Principle (DIP):**

Kelas WebService bergantung pada abstraksi (interface) daripada implementasi konkret.
Ini memungkinkan fleksibilitas dalam penerapan dan mengurangi ketergantungan langsung pada kelas-kelas implementasi, sehingga mempermudah pengujian dan perubahan implementasi di masa mendatang.

   Dengan menerapkan SOLID Design Principles, program memiliki desain yang modular, terpisah, dan mudah diperluas. Setiap repositori data memiliki tanggung jawab yang jelas dan dapat digunakan secara fleksibel dengan memanfaatkan polimorfisme dan abstraksi. Prinsip-prinsip tersebut membantu meningkatkan keterbacaan, pemeliharaan, dan fleksibilitas kode.
     
# No.4 Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
- **Command Pattern** dikarenakan dalam projek com.coc ini terdapat pula com.coc.swing. Yang dimana di dalamnya terdapat tombol-tombol yang berfungsi untuk menciptakan suatu interaksi antara aplikasi dan pengguna.
**_Link Source Code_** [Button](Button.java) dan [ButtonOutLine](ButtonOutLine.java)

- **Design Pattern** dikarenakan pada projek com.coc ini juga terdapat adanya design pattern yang terletak pada animasi antarmuka atau jendela awal aplikasi. Yang dimana aplikasi terkesan lebih eye cathcing dan menarik untuk dipandang.
**_Link Source Code_** [MyPasswordField](MyPasswordField.java), [MyTextField](MyTextField.java), dan [PanelRound](PanelRound.java)

# No.5 Mampu menunjukkan dan menjelaskan konektivitas ke database
- Berikut adalah codingan untuk konektivitas database, yang dimana tools yang digunakan adalah Apache Netbeans 18 dengan tools database nya yaitu Navicat server MySQL.
- Untuk konektivitas ini, saya mengimplementasikan yaitu user atau pengguna nanti menginputkan sebuah nama(username), alamat email, dan password. Kemudian, setelah semua berhasil diinputkan maka aplikasi dan database akan merespon dengan mengirimkan kode verifikasi ke email yang tertera. Setelah kode verifikasi pada email berhasil di dapatkan, maka kode tersebut akan dimasukkan ke kolom yang sudah ada pada aplikasi. 
- Setelah dinyatakan registrasi akun(sign up) berhasil, maka pada login(sign in) akun nanti tinggal kita masukkan akun yang sudah diregistrasi tadi agar dapat login aplikasi. 
- Konektivitas database ini memungkinkan kita untuk mengelola secara langsung data pengguna atau user agar tetap teratur dan terkelola dengan baik. Transparasi pun tetap didapatkan, namun privasi pengguna tetap diprioritaskan.

**Link Source Code:** [DatabaseConnection](DatabaseConnection.java)

# No.6 Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya


# No.7 Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
a. Window and Layout
- Jendela utama diwakili oleh "javax.swing.JFrame"
- Pengaturan tata letak dibuat dalam method "MigLayout", yang merupakan pengelola tata letak fleksibel yang memungkinkan pemosisian dan pengubahan ukuran komponen dengan mudah.

b. Panels
- GUI memiliki beberapa panel, seperti PanelCover, PanelLoading, PanelLoginAndRegister, dan PanelVerifyCode. Panel-panel tersebut tentunya mempunyai fungsi dan tujuan yang berbeda,
- "PanelCover" berfungsi sebagai layar penutup dan muncul di awal aplikasi,
- "PanelLoading" ditampilkan selama operasi asinkron, seperti mengirim email, untuk memberikan umpan balik visual kepada pengguna,
- "PanelVerifyCode" muncul saat pengguna mendaftar dan perlu memverifikasi alamat email mereka menggunakan kode yang dikirimkan melalui email, dan
- "PanelLoginAndRegister" menggabungkan formulir masuk dan pendaftaran, yang memungkinkan pengguna beralih antara mode masuk dan pendaftaran.

c. Animation
- GUI menggabungkan animasi untuk memberikan transisi yang mulus antara layar (panel) yang berbeda,
- Animasi ini dicapai dengan menggunakan kelas "TimingTarget" dan "Animator" yang disediakan oleh "org.jdesktop.animation.timing" package.

d. Buttons and Actions
- GUI memiliki tombol untuk masuk, mendaftar, dan memverifikasi kode email,
- Tindakan tombol ditangani oleh "ActionListener" implementasi, seperti "eventRegister", "eventLogin", dan "eventButtonOK".

e. User Service and Mail Service
- Aplikasi menggunakan "ServiceUser" dan "ServiceMail" kelas masing-masing untuk menangani operasi terkait pengguna dan pengiriman email,
- Registrasi pengguna, login, verifikasi email, dan penanganan kesalahan dilakukan melalui layanan ini.

f. Message Panel
- Program menampilkan pesan (sukses atau error) menggunakan "Message" panel.
- Pesan ditampilkan menggunakan animasi untuk muncul dan menghilang dengan lancar.

g. Main Method
- Metode "main" mengatur tampilan untuk aplikasi tersebut.
- Itu membuat instance kelas "Main", terhubung ke database, dan membuat jendela utama terlihat.

h. Overall Layout
- (JLayeredPane "bg") adalah wadah utama untuk komponen aplikasi.
- Panel dan komponen yang berbeda ditambahkan ke "bg" batasan penggunaan yang ditentukan dalam file "MigLayout".
- Tata letak dibagi menjadi persentase, memungkinkan pengubahan ukuran dan pemosisian komponen yang fleksibel.

**LINK Dokumentasi Output:** [Halaman Register](againfirst.png), [Halaman Register dengan Inputan](again1.png), [Halaman Animasi](again2.png), [Halaman Login](again3.png), dan [Halaman Login dengan Inputan](again4.png)

# No.8 Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital


# No.9 Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
[Link Demonstrasi Youtube](https://youtu.be/tUClTfmZy7k)

# No.10 BONUS !!! Mendemonstrasikan penggunaan Machine Learning

